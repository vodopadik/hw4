<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once 'src/functions.php';
require_once('src/support.php');
require_once('src/db.php');

$dataPost = $_POST;

if (isset($dataPost['send'])) {
    $userAvatar = $_FILES['userAvatar'];
    $resultForm = validateForm($dataPost, $userAvatar);
    if ($resultForm) {
        saveUser($dataPost);
        redirectToPage();
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Add User</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/style/main.css">
</head>
<body>
<div class="container-xl">
    <div class="table-responsive">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Add <b>Employee</b></h2>
                    </div>
                    <div class="col-sm-6">
                        <input type="submit" class="btn btn-success" value="Add" name="send" form="addUser">
                        <a href="/index.php" class="btn btn-primary">
                            <span>Return</span>
                        </a>
                    </div>
                </div>
            </div>
            <form method="post" action="add.php" enctype="multipart/form-data" id="addUser">
                <div class="form-group">
                    <label for="userName">Name</label>
                    <input type="text" class="form-control" id="userName" name="userName">
                    <?= showErrors('userName'); ?>
                </div>
                <div class="form-group">
                    <label for="userEmail">Email</label>
                    <input type="email" class="form-control" id="userEmail" name="userEmail">
                    <?= showErrors('userEmail'); ?>
                </div>
                <div class="form-group">
                    <label for="userAddress">Address</label>
                    <textarea class="form-control" id="userAddress" name="userAddress"></textarea>
                    <?= showErrors('userAddress'); ?>
                </div>
                <div class="form-group">
                    <label for="userPhone">Phone</label>
                    <input type="text" class="form-control" id="userPhone" name="userPhone">
                    <?= showErrors('userPhone'); ?>
                </div>
                <div class="form-group">
                    <label for="userAvatar">Avatar</label>
                    <input type="file" class="form-control" id="userAvatar" name="userAvatar">
                    <?= showErrors('userAvatar'); ?>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
</body>
</html>