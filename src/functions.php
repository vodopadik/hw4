<?php
require_once 'support.php';
require_once 'db.php';

function validateForm(array $dataPost, array $userAvatar): bool
{
    if (empty($dataPost['userName'])) {
        addError('userName', 'Name is Empty!');
    } elseif (strlen($dataPost['userName']) > 25) {
        addError('userName', 'Name must be less than 25 characters!');
    }

    if (empty($dataPost['userEmail'])) {
        addError('userEmail', 'Email is Empty');
    } elseif (!filter_var($dataPost['userEmail'], FILTER_VALIDATE_EMAIL)) {
        addError('userEmail', 'It is not email');
    }

    if (!empty($dataPost['userAddress']) && strlen($dataPost['userAddress']) > 100) {
        addError('userAddress', 'Long');
    }

    if (empty($dataPost['userPhone'])) {
        addError('userPhone', 'Empty');
    } elseif (!is_numeric($dataPost['userPhone'])) {
        addError('userPhone', 'It is not Number');
    }

    if (!empty($userAvatar)) {
        validateAvatar($userAvatar);
    }

    if (checkValidateErrors() == false) {
        return true;
    }
    return false;
}

function validateAvatar($userAvatar): bool
{
    if (!empty($userAvatar)) {
        $allowedMimeTypes = ['image/gif', 'image/png', 'image/jpeg', 'image/jpg'];
        if (!empty($userAvatar['tmp_name']) && !in_array($userAvatar['type'], $allowedMimeTypes)) {
            addError('userAvatar', 'This type not allowed');
        } elseif (getimagesize($userAvatar['tmp_name'])[0] > 100 and getimagesize($userAvatar['tmp_name'])[1] > 100) {
            addError('userAvatar', '100x100');
        } else {
            saveAvatar($userAvatar);
        }
    }
    if (checkValidateErrors() == false) {
        return true;
    }
    return false;
}

function saveAvatar($userAvatar): bool
{
    $name = basename($userAvatar['name']);
    $info = explode('.', $name);
    $extension = $info[1];
    $newName = md5($name . time()) . '.' . $extension;
    $path = 'upload/' . $newName;
    $_SESSION['name'] = $newName;
    if (!move_uploaded_file($userAvatar['tmp_name'], $path)) {
        addError('userAvatar', 'Something gone wrong');
    }
    if (checkValidateErrors() == false) {
        return true;
    }
    return false;
}

function saveUser($dataPost)
{
    $users = [
        'name' => $dataPost['userName'],
        'email' => $dataPost['userEmail'],
        'address' => $dataPost['userAddress'],
        'phone' => $dataPost['userPhone'],
        'avatar' => $_SESSION['name']
    ];
    addUser($users);
}



