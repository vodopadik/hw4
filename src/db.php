<?php

function getAllUsers(): array
{
    $pdo = getPDO();
    $stm = $pdo->query('SELECT * FROM users');
    $users = $stm->fetchAll();
    return $users;
}

function addUser(array $user): bool
{
    $pdo = getPDO();
    $sql = 'INSERT INTO users (name, email, address, phone, avatar) VALUE (?,?,?,?,?)';
    $stm = $pdo->prepare($sql);
    $stm->execute([$user['name'], $user['email'], $user['address'], $user['phone'], $user['avatar']]);
    return true;
}

function deleteUser(string $emailUser): bool
{
    $pdo = getPDO();
    $stm = $pdo->prepare('DELETE FROM users WHERE email = ?');
    $stm->execute([$emailUser]);
}

function getPDO(): PDO
{
    static $pdo;
    if ($pdo == null) {
        $dsn = 'mysql:host=localhost;dbname=hw17';
        $pdo = new PDO($dsn, 'mysql', '');
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    }
    return $pdo;
}