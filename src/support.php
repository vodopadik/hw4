<?php
require_once 'functions.php';

function checkValidateErrors(): bool
{
    if (!empty($_SESSION['errors'])) {
        return true;
    }
    return false;
}

function addError(string $field, string $message)
{
    if (empty($_SESSION['errors'])) {
        $_SESSION['errors'] = [];
    }
    $_SESSION['errors'][$field][] = $message;
}

function showErrors(string $field)
{
    if (!empty($_SESSION['errors'][$field])) {
        foreach ($_SESSION['errors'][$field] as $error) {
            echo '<p style="color:red;">' . $error . '</p>';
        }
        unset($_SESSION['errors'][$field]);
    }
}

function formatPhone($phone)
{
    return $phone = '+' . $phone[0] . $phone[1] . '(' . $phone[2] . $phone[3] . $phone[4] . ')' . $phone[5] . $phone[6] .
        $phone[7] . '-' . $phone[8] . $phone[9] . '-' . $phone[10] . $phone[11];
}

function redirectToPage(string $page = '/')
{
    header('Location: ' . $page);
    exit;
}
