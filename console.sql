create database hw17;
use hw17;

create table posts (
                       id               int auto_increment primary key,
                       name             varchar(255) not null,
                       description      text         not null
);

create table posts_meta (
    id int(10) auto_increment primary key,
    meta_name        varchar(250) null,
    meta_description varchar(255) null,
    meta_keywords    varchar(255) null,
    id_post int (10),
    foreign key(id_post) references posts(id)on delete restrict on update restrict
);

create table posts_images (
    id int(10) auto_increment primary key,
    image varchar(255) not null,
    id_post int (10),
    foreign key(id_post) references posts(id) on delete restrict on update restrict
);

create table tags (
    id int(10) auto_increment primary key,
    tag varchar(255) not null
);

create table posts_tags (
    id int(10) auto_increment primary key,
    id_tag int(10),
    foreign key (id_tag) references tags(id) on delete restrict on update restrict,
    id_post int(10),
    foreign key (id_post) references posts (id) on delete restrict on UPDATE restrict
);

insert posts(name, description) values
('news 1', 'description of news 1'),
('news 2', 'description of news 2'),
('news 3', 'description of news 3');

insert posts_images(image, id_post) values
('image1.png', 1),
('image2.png', 1),
('image3.png', 2),
('image4.png', 2),
('image5.png', 3);

insert tags(tag) values
('tag 1'),
('tag 2'),
('tag 3');

insert posts_tags(id_tag, id_post) values
(1, 1),
(2, 1),
(1, 2),
(3, 3);

select * from tags;

select tags.tag, p.name, p.description from tags
left join posts_tags pt on tags.id = pt.id_tag
left join posts p on p.id = pt.id_post;
